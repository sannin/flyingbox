
// --------------------------------------------- //
// ------- Random Game Kristofer Brink --------- //
// -------- Created by Nikhil Suresh ----------- //
// -------- Three.JS is by Mr. doob  ----------- //
// --------------------------------------------- //

// ------------------------------------- //
// ------- GLOBAL VARIABLES ------------ //
// ------------------------------------- //

// scene object variables
var renderer, scene, camera, pointLight, spotLight;
var playerBox;

var myLoadImage, myLoadTexture;
(function (){
    var imageLoader = new THREE.ImageLoader();
    myLoadImage = function () {
        arguments[0] = 'images/' + arguments[0];
        return imageLoader.load.apply(imageLoader, arguments);
    };
    
    var textureLoader = new THREE.TextureLoader();
    myLoadTexture = function () {
        arguments[0] = 'images/' + arguments[0];
        return textureLoader.load.apply(textureLoader, arguments);
    };
})();

// ------------------------------------- //
// ------- GAME FUNCTIONS -------------- //
// ------------------------------------- //

function setup()
{
	// Set up physics
	Physijs.scripts.worker = 'Scripts/physijs_worker.js';
    Physijs.scripts.ammo = 'ammo.js';
    
	// set up all the 3D objects in the scene	
	createScene();
	
	// and let's get cracking!
	draw();
}

function setSceneBasics(){
    // set the scene size
	var WIDTH = window.innerWidth;
	var HEIGHT = window.innerHeight;

	var c = document.getElementById("gameCanvas");

	// create a WebGL renderer, camera
	// and a scene
	renderer = new THREE.WebGLRenderer({antialias: true, logarithmicDepthBuffer: true});
    
    // Make camera
	camera =
	  new THREE.PerspectiveCamera(
		50, // View angle
		WIDTH / HEIGHT, // Aspect
		0.1, // Near
		3000000); // Far
    // rotate to face towards the opponent
    camera.rotation.x = -30 * Math.PI/180;
	camera.rotation.y = -30 * Math.PI/180;
    

	scene = new Physijs.Scene();

	// add the camera to the scene
	scene.add(camera);
	
	// set a default position for the camera
	// not doing this somehow messes up shadow rendering well this doesn't do anything says Kristofer Brink
	//camera.position.z = 200;
	
	// start the renderer
	renderer.setSize(WIDTH, HEIGHT);

	// attach the render-supplied DOM element
	c.appendChild(renderer.domElement);
}

function setGravity() {
    scene.setGravity(new THREE.Vector3( 0, -100, 0 ));
}

function makePlayerBox(){
     // ------------------------------------------- playerBox
	// // create the playerBox's material
    
    // Materials
    var playerBoxMaterial = Physijs.createMaterial(
        new THREE.MeshPhongMaterial({ map: myLoadTexture( 'textures/wood.jpg' ) }),
        .8, // high friction
        .2 // low restitution
    );
    playerBoxMaterial.map.wrapS = playerBoxMaterial.map.wrapT = THREE.RepeatWrapping;
    playerBoxMaterial.map.repeat.set( 3, 3 );
		
	
	playerBox = new Physijs.BoxMesh( 

	   new THREE.BoxGeometry( 
		  100, 
		  10, 
		  100), 
        playerBoxMaterial,
        1); // Gravity

	playerBox.receiveShadow = true;
    playerBox.castShadow = true;
    playerBox.position.y = 420;
    playerBox.position.z = 100;
	// // add the sphere to the scene
	scene.add(playerBox);
	
    
    playerBox.setAngularFactor(new THREE.Vector3(2,2,2));
    
    playerBox.rotation.set(new THREE.Vector3(0, 25, 90));
}

function makePillars(){
    // --------------------------------------------------------------------------
	
	// we iterate 10x (5x each side) to create pillars to show off shadows
	// this is for the pillars on the right
    // ------------------------------------------- Create material 
	// create the pillar's material
     // Materials
    pillarMaterial = Physijs.createMaterial(
        
        new THREE.MeshLambertMaterial({ map: myLoadTexture( 'textures/wood.jpg' ) }),
        .8, // high friction
        1 // low restitution
    );
    pillarMaterial.map.wrapS = pillarMaterial.map.wrapT = THREE.RepeatWrapping;
    pillarMaterial.map.repeat.set( 3, 3);
    
    // ------------------------------------------- ^
    
    for (i = 0; i < 5; i++){
		var backdrop = new Physijs.BoxMesh(

		  new THREE.CubeGeometry( 
		  10000, 
		  100 * (1+i), 
		  500, 
		  1, 
		  1,
		  1 ),

		  pillarMaterial, 0);
		  
		backdrop.position.x = -50 + 1 * 100;
		backdrop.position.y = 300 + i * 100;
		backdrop.position.z = -30 - i * 500;
		backdrop.castShadow = true;
		backdrop.receiveShadow = true;		
		scene.add(backdrop);	
    }
}

function makePointLight(){
    // // create a point light
	pointLight =
	  new THREE.PointLight(0xffffff, 
                           1, // Intensity
                           2000, // Distance
                           4); // decay

	// set its position
	pointLight.position.x = 100;
	pointLight.position.y = 1000;
	pointLight.position.z =  0;
	pointLight.intensity = 50;
    pointLight.castShadow = true;
	// add to the scene
	scene.add(pointLight);
}

function makeSpotlight(){
    // add a spot light
	// this is important for casting shadows
    spotLight = new THREE.SpotLight(0xffffff, 
                                    4, // Intensity
                                    20000, //distance
                                    3, // angle
                                    1, // exponent
                                    1); // decay
   // (0xffffff, 1000, 3, 100 * math.PI/180, 2, 1)
    spotLight.position.set(0, 0, 500);
    spotLight.castShadow = true;
    scene.add(spotLight);
}
/*
funciton makeTextMesh(){
    var i = 1;
}

function makeText3D(){
    var textMaterial = new THREE.MeshPhongMaterial({
        color: 0xdddddd
    });
    var textGeom = new THREE.TextGeometry( 'Hello World!', {
        font: 'harabara' // Must be lowercase!
        , size: 10
        , height: 10
    });
    var textMesh = new THREE.Mesh( textGeom, textMaterial );

    scene.add( textMesh );
}
*/

function makeSkyBox(){
    var cubeMap = new THREE.CubeTexture( [] );
    cubeMap.format = THREE.RGBFormat;

    myLoadImage( 'textures/sky/skyboxsun25degtest.png', function ( image ) {
        if (!image)
            throw new Error('Unable to load something.');
        console.log(image);

        var getSide = function ( x, y ) {

            var size = 1024;

            var canvas = document.createElement( 'canvas' );
            canvas.width = size;
            canvas.height = size;

            var context = canvas.getContext( '2d' );
            context.drawImage( image, - x * size, - y * size );

            return canvas;

        };

        cubeMap.images[ 0 ] = getSide( 2, 1 ); // px
        cubeMap.images[ 1 ] = getSide( 0, 1 ); // nx
        cubeMap.images[ 2 ] = getSide( 1, 0 ); // py
        cubeMap.images[ 3 ] = getSide( 1, 2 ); // ny
        cubeMap.images[ 4 ] = getSide( 1, 1 ); // pz
        cubeMap.images[ 5 ] = getSide( 3, 1 ); // nz
        cubeMap.needsUpdate = true;      

    } );

    var cubeShader = THREE.ShaderLib[ 'cube' ];
    cubeShader.uniforms[ 'tCube' ].value = cubeMap;

    var skyBoxMaterial = new THREE.ShaderMaterial( {
        fragmentShader: cubeShader.fragmentShader,
        vertexShader: cubeShader.vertexShader,
        uniforms: cubeShader.uniforms,
        depthWrite: false,
        side: THREE.BackSide
    } );

    var skyBox = new THREE.Mesh(
        new THREE.BoxGeometry( 1000000, 1000000, 1000000 ),
        skyBoxMaterial
    );

    scene.add( skyBox );
}

var light;

function makeWater(){
    var parameters = {
        width: 500,
        height: 500,
        widthSegments: 25,
        heightSegments: 25,
        depth: 10,
        param: 4,
        filterparam: 1
    };
    
    light = new THREE.DirectionalLight( 0xffffbb, 1 );
    light.position.set( - 1, 1600, - 1 );
    light.castShadow = true;
    light.shadowCameraNear = 1;
    light.shadowCameraFar = 4200;
    light.shadowMapWidth = 8192;
    light.shadowMapHeight = 8192;
    light.shadowCameraLeft = -100;
    light.shadowCameraRight = 100;
    light.shadowCameraTop = 100;
    light.shadowCameraBottom = -100;
    light.shadowCameraVisible = true;
    scene.add( light );
    scene.add(new THREE.CameraHelper(light.shadow.camera));

    
    waterNormals = myLoadTexture( 'textures/liquids/waternormals.jpg' );
    waterNormals.wrapS = waterNormals.wrapT = THREE.RepeatWrapping;

    water = new THREE.Water( renderer, camera, scene, {
        textureWidth: 512,
        textureHeight: 512,
        waterNormals: waterNormals,
        alpha: 	1.0,
        sunDirection: light.position.clone().normalize(),
        sunColor: 0xffffff,
        waterColor: 0x001e0f,
        distortionScale: 50.0,
    } );


    mirrorMesh = new THREE.Mesh(
        new THREE.PlaneBufferGeometry( parameters.width * 500, parameters.height * 500 ),
        water.material
    );

    mirrorMesh.add( water );
    mirrorMesh.rotation.x = - Math.PI * 0.5;
   scene.add( mirrorMesh );

}

postprocessing = { enabled : true };
bgColor = 0x000511;
sunColor = 0xffee00;

function makeGodRays(){
    postprocessing.scene = new THREE.Scene();

    postprocessing.camera = new THREE.OrthographicCamera( window.innerWidth / - 2, window.innerWidth / 2,                    window.innerHeight / 2, window.innerHeight / - 2, -10000, 10000 );
    postprocessing.camera.position.z = 100;

    postprocessing.scene.add( postprocessing.camera );

    var pars = { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter, format: THREE.RGBFormat };
    postprocessing.rtTextureColors = new THREE.WebGLRenderTarget( window.innerWidth, window.innerHeight, pars );

    // Switching the depth formats to luminance from rgb doesn't seem to work. I didn't
    // investigate further for now.
    // pars.format = THREE.LuminanceFormat;

    // I would have this quarter size and use it as one of the ping-pong render
    // targets but the aliasing causes some temporal flickering

    postprocessing.rtTextureDepth = new THREE.WebGLRenderTarget( window.innerWidth, window.innerHeight, pars );

    // Aggressive downsize god-ray ping-pong render targets to minimize cost

    var w = window.innerWidth / 4.0;
    var h = window.innerHeight / 4.0;
    postprocessing.rtTextureGodRays1 = new THREE.WebGLRenderTarget( w, h, pars );
    postprocessing.rtTextureGodRays2 = new THREE.WebGLRenderTarget( w, h, pars );

    // god-ray shaders

    var godraysGenShader = THREE.ShaderGodRays[ "godrays_generate" ];
    postprocessing.godrayGenUniforms = THREE.UniformsUtils.clone( godraysGenShader.uniforms );
    postprocessing.materialGodraysGenerate = new THREE.ShaderMaterial( {

        uniforms: postprocessing.godrayGenUniforms,
        vertexShader: godraysGenShader.vertexShader,
        fragmentShader: godraysGenShader.fragmentShader

    } );

    var godraysCombineShader = THREE.ShaderGodRays[ "godrays_combine" ];
    postprocessing.godrayCombineUniforms = THREE.UniformsUtils.clone( godraysCombineShader.uniforms );
    postprocessing.materialGodraysCombine = new THREE.ShaderMaterial( {

        uniforms: postprocessing.godrayCombineUniforms,
        vertexShader: godraysCombineShader.vertexShader,
        fragmentShader: godraysCombineShader.fragmentShader

    } );

    var godraysFakeSunShader = THREE.ShaderGodRays[ "godrays_fake_sun" ];
    postprocessing.godraysFakeSunUniforms = THREE.UniformsUtils.clone( godraysFakeSunShader.uniforms );
    postprocessing.materialGodraysFakeSun = new THREE.ShaderMaterial( {

        uniforms: postprocessing.godraysFakeSunUniforms,
        vertexShader: godraysFakeSunShader.vertexShader,
        fragmentShader: godraysFakeSunShader.fragmentShader

    } );

    postprocessing.godraysFakeSunUniforms.bgColor.value.setHex( bgColor );
    postprocessing.godraysFakeSunUniforms.sunColor.value.setHex( sunColor );

    postprocessing.godrayCombineUniforms.fGodRayIntensity.value = 0.75;

    postprocessing.quad = new THREE.Mesh(
        new THREE.PlaneBufferGeometry( window.innerWidth, window.innerHeight ),
        postprocessing.materialGodraysGenerate
    );
    postprocessing.quad.position.z = -9900;
    postprocessing.scene.add( postprocessing.quad );
}

function createScene()
{
    
    setSceneBasics();
    
    setGravity();
   
    makePlayerBox();
    
    //makeText3D();
    
    //scene.add( new THREE.AmbientLight( 0x404040 ) );
    
    makeWater();
    
    makeSkyBox();
    
    makePillars(); 
    
    makeGodRays();
	
	//makePointLight();
	
	//makeSpotlight();
	
	// MAGIC SHADOW CREATOR DELUXE EDITION with Lights PackTM DLC
	renderer.shadowMap.enabled = true;
    //renderer.shadowMapType = THREE.PCFSoftShadowMap; // snooth shadows

    /* Call the playerBallMovement() once every physics (simulation) frame. */
    scene.addEventListener('update', animate);
}

function draw()
{	
	// Physijs simulate
    if(!Key.isDown(Key.F)) {
	   scene.simulate(); // run physics
    }
	// draw THREE.JS scene
    
    cameraPosition();
    
     
    
    // water
    water.material.uniforms.time.value += 1.0 / 60.0;
	
    water.render();
    
    // God Ray
    renderGodRay();
    
    // Update light
    light.updateMatrix();
    
    
    
    
	renderer.render(scene, camera);
    
    
    // loop draw function call
	requestAnimationFrame(draw);
}




// Handles player's movement
function animate()
{
    // --------------------- Player movement ------
	// move left
	if (Key.isDown(Key.A))		
	{
		// Move left
		playerBox.applyCentralForce(new THREE.Vector3(-1000, 0, 0));
	}	
	// move right
	if (Key.isDown(Key.D))
	{
		playerBox.applyCentralForce(new THREE.Vector3(1000, 0, 0));
	}
    if (Key.isDown(Key.W))
    {
        playerBox.applyCentralForce(new THREE.Vector3(0, 0, -1000));
    }
    if (Key.isDown(Key.S))
    {
        playerBox.applyCentralForce(new THREE.Vector3(0, 0, 1000));
    }
    if(Key.isDown(Key.SPACE)) {
        playerBox.applyCentralForce(new THREE.Vector3(0,2000,0));
    }
    
    
    
    // Rotational forces 
    var boxSideLengthHalf = 50;
    var strength = 25;
    // Position and rotation
    if (Key.isDown(Key.Z)) {
        playerBox.applyForce(new THREE.Vector3(0, strength, 0).applyEuler(playerBox.rotation), new THREE.Vector3(-boxSideLengthHalf, 50, -boxSideLengthHalf).applyEuler(playerBox.rotation).add(playerBox.position));
    }
    if (Key.isDown(Key.X)) {
        playerBox.applyForce(new THREE.Vector3(0, strength, 0).applyEuler(playerBox.rotation), new THREE.Vector3(boxSideLengthHalf, 50, -boxSideLengthHalf).applyEuler(playerBox.rotation).add(playerBox.position));
    }
    if (Key.isDown(Key.C)) {
        playerBox.applyForce(new THREE.Vector3(0, strength, 0).applyEuler(playerBox.rotation), new THREE.Vector3(-boxSideLengthHalf, 50, boxSideLengthHalf).applyEuler(playerBox.rotation).add(playerBox.position));
    }
    if (Key.isDown(Key.V)) {
        playerBox.applyForce(new THREE.Vector3(0, strength, 0).applyEuler(playerBox.rotation), new THREE.Vector3(boxSideLengthHalf, 50, boxSideLengthHalf).applyEuler(playerBox.rotation).add(playerBox.position));
    }
    /*
	paddle1.scale.y += (1 - paddle1.scale.y) * 0.2;	
	paddle1.scale.z += (1 - paddle1.scale.z) * 0.2;	
	paddle1.position.y += paddle1DirY;
	*/
	
    
}

// Handles camera and lighting logic
function cameraPosition()
{
	// we can easily notice shadows if we dynamically move lights during the game
	//spotLight.position.x = playerBox.position.x;
   // spotLight.position.y = playerBox.position.y + 200;
	//spotLight.position.z = playerBox.position.z;
	// Camera movement IJKL
    if (Key.isDown(Key.I)) {
        camera.rotation.x += 1 * Math.PI/180;
    }
    if (Key.isDown(Key.K)) {
        camera.rotation.x -= 1 * Math.PI/180;
    }
    if (Key.isDown(Key.J)) {
        camera.rotation.y  += 1 * Math.PI/180;
    }
    if (Key.isDown(Key.L)) {
        camera.rotation.y -= 1 * Math.PI/180;
    }
	// move to behind the player's paddle
	camera.position.x = playerBox.position.x - 300;
	camera.position.y = playerBox.position.y + 400;
	camera.position.z = playerBox.position.z + 500;
	
   
}

var screenSpacePosition = new THREE.Vector3();
var sunPosition = new THREE.Vector3( 0, 1000, 10 );
var materialDepth;

function renderGodRay() {
    if ( postprocessing.enabled ) {

        // Find the screenspace position of the sun

        screenSpacePosition.copy( sunPosition ).project( camera );

        screenSpacePosition.x = ( screenSpacePosition.x + 1 ) / 2;
        screenSpacePosition.y = ( screenSpacePosition.y + 1 ) / 2;

        // Give it to the god-ray and sun shaders

        postprocessing.godrayGenUniforms[ "vSunPositionScreenSpace" ].value.x = screenSpacePosition.x;
        postprocessing.godrayGenUniforms[ "vSunPositionScreenSpace" ].value.y = screenSpacePosition.y;

        postprocessing.godraysFakeSunUniforms[ "vSunPositionScreenSpace" ].value.x = screenSpacePosition.x;
        postprocessing.godraysFakeSunUniforms[ "vSunPositionScreenSpace" ].value.y = screenSpacePosition.y;

        // -- Draw sky and sun --

        // Clear colors and depths, will clear to sky color

        renderer.clearTarget( postprocessing.rtTextureColors, true, true, false );

        // Sun render. Runs a shader that gives a brightness based on the screen
        // space distance to the sun. Not very efficient, so i make a scissor
        // rectangle around the suns position to avoid rendering surrounding pixels.

        var sunsqH = 0.74 * window.innerHeight; // 0.74 depends on extent of sun from shader
        var sunsqW = 0.74 * window.innerHeight; // both depend on height because sun is aspect-corrected

        screenSpacePosition.x *= window.innerWidth;
        screenSpacePosition.y *= window.innerHeight;

        renderer.setScissor( screenSpacePosition.x - sunsqW / 2, screenSpacePosition.y - sunsqH / 2, sunsqW, sunsqH );
        renderer.enableScissorTest( true );

        postprocessing.godraysFakeSunUniforms[ "fAspect" ].value = window.innerWidth / window.innerHeight;

        postprocessing.scene.overrideMaterial = postprocessing.materialGodraysFakeSun;
        renderer.render( postprocessing.scene, postprocessing.camera, postprocessing.rtTextureColors );

        renderer.enableScissorTest( false );

        // -- Draw scene objects --

        // Colors

        scene.overrideMaterial = null;
        renderer.render( scene, camera, postprocessing.rtTextureColors );

        // Depth

        scene.overrideMaterial = materialDepth;
        renderer.render( scene, camera, postprocessing.rtTextureDepth, true );

        // -- Render god-rays --

        // Maximum length of god-rays (in texture space [0,1]X[0,1])

        var filterLen = 1.0;

        // Samples taken by filter

        var TAPS_PER_PASS = 6.0;

        // Pass order could equivalently be 3,2,1 (instead of 1,2,3), which
        // would start with a small filter support and grow to large. however
        // the large-to-small order produces less objectionable aliasing artifacts that
        // appear as a glimmer along the length of the beams

        // pass 1 - render into first ping-pong target

        var pass = 1.0;
        var stepLen = filterLen * Math.pow( TAPS_PER_PASS, -pass );

        postprocessing.godrayGenUniforms[ "fStepSize" ].value = stepLen;
        postprocessing.godrayGenUniforms[ "tInput" ].value = postprocessing.rtTextureDepth;

        postprocessing.scene.overrideMaterial = postprocessing.materialGodraysGenerate;

        renderer.render( postprocessing.scene, postprocessing.camera, postprocessing.rtTextureGodRays2 );

        // pass 2 - render into second ping-pong target

        pass = 2.0;
        stepLen = filterLen * Math.pow( TAPS_PER_PASS, -pass );

        postprocessing.godrayGenUniforms[ "fStepSize" ].value = stepLen;
        postprocessing.godrayGenUniforms[ "tInput" ].value = postprocessing.rtTextureGodRays2;

        renderer.render( postprocessing.scene, postprocessing.camera, postprocessing.rtTextureGodRays1  );

        // pass 3 - 1st RT

        pass = 3.0;
        stepLen = filterLen * Math.pow( TAPS_PER_PASS, -pass );

        postprocessing.godrayGenUniforms[ "fStepSize" ].value = stepLen;
        postprocessing.godrayGenUniforms[ "tInput" ].value = postprocessing.rtTextureGodRays1;

        renderer.render( postprocessing.scene, postprocessing.camera , postprocessing.rtTextureGodRays2  );

        // final pass - composite god-rays onto colors

        postprocessing.godrayCombineUniforms["tColors"].value = postprocessing.rtTextureColors;
        postprocessing.godrayCombineUniforms["tGodRays"].value = postprocessing.rtTextureGodRays2;

        postprocessing.scene.overrideMaterial = postprocessing.materialGodraysCombine;

        renderer.render( postprocessing.scene, postprocessing.camera );
        postprocessing.scene.overrideMaterial = null;
    }
}

